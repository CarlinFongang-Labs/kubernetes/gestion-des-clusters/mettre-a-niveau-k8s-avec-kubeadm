# Mettre à niveau k8s avec kubeadm

------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______ 

# Objectifs

Lors de l'exploitation de Kubernetes, il sera nécessaire de façon périodique, d'effectuer des mises à niveau de celui-ci afin de maintenir les cluste à jour.

L'objectif de ce lab sera de : 

- Comprendre le Processus de mise à niveau avec kubeadm

- Lister les étapes de mise à niveau du **Control Plan**

- Aperçu des étapes de mise à niveau des **workers nodes**

- Démonstration Pratique

# Étapes de mise à niveau du Control Plan

Nous commencerons par 

1. Mettre à niveau kubeadm sur le **Contrl plane node**

2. Nous drainerons le **control plane node** [comme vu dans le lab sur le drainage d'un node](https://gitlab.com/CarlinFongang-Labs/kubernetes/gestion-des-clusters/lab-drainer-un-noeud-k8s.git)

3. Nous planifirons notre mise à niveau avec la commande `kubeadmin upgrade plan`

4. Nous utiliserons ensuite la commande `kubeadm upgrade apply` pour appliquer la mise à niveau

5. Nous mettrons à jour **kubelete** et **kubectl** sur le **control node plan**

6. Enfin nous allons joindre le **control plan node** de nouveau au cluster grace à la commande `kubectl uncordon` 

# Étapes de mise à niveau des Workers

1. Drainage des nodes

2. Mise à niveau de kubeadm

3. Mise à niveau des configuration de kubelet (kubeadm upgrade node)

4. Mise à niveau de kubelet et kubectl

5. Changement du statut des nodes pour la reception de nouvealle charge de travail


# 1. Mise à niveau du Control Plan node

## 1. Drainage du Control plan node
Nous allons commencer par nous connecter au control plan que nous allons drainer par la suite 

````bash
ssh -i id_rsa user@public_ip_address
````

````bash
kubectl drain k8s-control --ignore-daemonsets
````

>![Alt text](img/image.png)
*Le node est à présent drainé*

il est possible de confirmer cela en vérifiant le status du node


````bash
kubectl get nodes
````

>![Alt text](img/image-1.png)
*Le noeud n'est plus disponible pour recevoir des workloads*

## 2. Mise à niveau de kubeadm


````bash
sudo apt-get update && \
sudo apt-get install -y --allow-change-held-packages kubeadm=1.27.2-1.1
````

>![Alt text](img/image-2.png)
*Mise à niveau de kubeadm effectuée*

L'on peut vérifier la nouvelle version 

````bash
kubeadm version
````

>![Alt text](img/image-3.png)
*Version 1.27.2-1.1 installée*

Nous venons d'effectuer une mise à niveau de la version 1.27.0 vers la 1.27.2.~

## 3. Plan de mise à niveau kubeadm

````bash
sudo kubeadm upgrade plan v1.27.2
````
>![Alt text](img/image-4.png)

Cette commande permet de crée un plan de différents composants internes qui doivent être mis à jour


## 4. Mise à niveau des composants du Control Plan

Nous allons reprendre la commande `sudo kubeadm upgrade apply <version>` affiché sur le précédent écran et à la suite du messa "You can now apply the upgrade by executing the following command:"

````bash
sudo kubeadm upgrade apply v1.27.2 -y
````
>![Alt text](img/image-5.png)
*Mise à niveau réussie*

A présent que kubeadm eest mis à jour, nous allons mettre à jour kubelet et kubectl sur le Control plan également

## 5. Mise à jour de kubelet et kubectl sur le Control node

````bash
sudo apt-get update && \
sudo apt-get install -y --allow-change-held-packages kubelet=1.27.2-1.1 kubectl=1.27.2-1.1
````

Le rôle de l'option `--allow-change-held-packages` dans les commandes de mise à jour des services Kubernetes est de permettre la mise à jour des packages qui ont été précédemment marqués comme "retenus" (held) pour empêcher leur mise à jour automatique de ceux-ci.

>![Alt text](img/image-6.png)
*Mise à jour de kubelet et kubectl*

## 6. Redémarrage de kubelet


````bash
sudo systemctl daemon-reload
sudo systemctl restart kubelet
````

A présent nous avons terminée la mise à niveau du Control plan node, nous pouvons à présent raccorder ce noeud au cluster afin de le rendre de nouveau disponible pour la pise en charge de nouveau workload

````bash
kubectl uncordon k8s-control
````
Une fois le status du Control node changé, nous pouvons vérifier cela en anttrant la commande :

````bash
kubectl get nodes 
````

>![Alt text](img/image-7.png)
*Liste des noeud du cluster*

Le control node est de nouveau disponible est et la version des composante (v1.27.2) et plus récente que celle des workers (v1.27.0) qui n'ont pas encore subis de mise à niveau.


# 2. Mise à niveau du Worker node 1

## 1. Drainage du Worker node 1

Dépuis le terminal du Control node, nous allons passer la commande suivante : 

````bash
kubectl drain k8s-worker1 --ignore-daemonsets --force
````

>![Alt text](img/image-8.png)
*Worker node 1 drainé*


## 2. Mise à niveau de kubeadm sur le worker 1

Une fois connecté au worker node 1, nous allons entrer la commande suivante : 

````bash
sudo apt-get update && \
sudo apt-get install -y --allow-change-held-packages kubeadm=1.27.2-1.1
````

>![Alt text](img/image-9.png)
*kubeadm mise à jour*

````
kubeadm version
````

>![Alt text](img/image-10.png)
*Vérification de la version de kubeadm installé*

## 3. Mise à niveau des cofigurations de kubelet sur le worker

````bash
sudo kubeadm upgrade node
````

>![Alt text](img/image-10.png)
*Mise à niveu des configuration de kubelet*


## 4. Mise à niveau de kubelet et kubectl sur le worker

````bash
sudo apt-get update && \
sudo apt-get install -y --allow-change-held-packages kubelet=1.27.2-1.1 kubectl=1.27.2-1.1
````

>![Alt text](img/image-11.png)
*Mise à niveau de kubelet et kubectl*


## 5. Redemarrage de kubelet

````bash
sudo systemctl daemon-reload && sudo systemctl restart kubelet
````
Une fois rendu à cette étape, nous allons mettre à jour le status de disponibilité du worker node 1, pour cela, nous allons de nouveau connecté au Control node.

## 6. Mise à jour du status de disponibilité du worker dans le cluster

A partir du terminal depuis le Control node, nous allons entrée les commandes suivantes : 

````bash
kubectl uncordon k8s-worker1
````

## 7. Vérification du status du worker node

````bash
kubectl get nodes
````
>![Alt text](img/image-12.png)
*Le worker node 1 est de nouveau disponible*

Le worker node 1 est de nouveau disponible et peut traiter à présent de nouvelle charge de travail. Les composant ont également été mise à niveau vers la 1.27.0 comme sur le Control node.

# 2. Mise à niveau du Worker node 2
Nous allons simplement reprendre le protocol appliqué pour la mise à niveau du worker 1, présenté plus haut.

## 1. Drenage du worker node 2

>![Alt text](img/image-13.png)
*Drenage du worker node 2*

## 2. Mise à niveau du worker node 2
>![Alt text](img/image-14.png)
*Worker node 2 mis à niveau*


# Reférence

[Mise à niveau de clusters sous kubeadm :](https://kubernetes.io/docs/tasks/administer-cluster/kubeadm/kubeadm-upgrade/) https://kubernetes.io/docs/tasks/administer-cluster/kubeadm/kubeadm-upgrade/

